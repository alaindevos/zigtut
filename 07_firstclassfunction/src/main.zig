const std = @import("std");

// Define a function type for operations on integers
const Operation = fn (a: i32, b: i32) i32;

// Define functions that conform to the Operation type
fn add(a: i32, b: i32) i32 {
    return a + b;
}

fn subtract(a: i32, b: i32) i32 {
    return a - b;
}

// A function that takes an Operation function as an argument
fn perform_operation(op: Operation, a: i32, b: i32) i32 {
    return op(a, b);
}

pub fn main() void {
    const result_add = perform_operation(add, 5, 3);
    std.debug.print("Result of addition: {}\n", .{result_add});

    const result_subtract = perform_operation(subtract, 5, 3);
    std.debug.print("Result of subtraction: {}\n", .{result_subtract});
}

test "simple test" {
    var list = std.ArrayList(i32).init(std.testing.allocator);
    defer list.deinit(); // try commenting this out and see if zig detects the memory leak!
    try list.append(42);
    try std.testing.expectEqual(@as(i32, 42), list.pop());
}
