const std = @import("std");

// Define a struct to represent a 2D point
const Point = struct {
    x: f32,
    y: f32,
    // Method to calculate the distance from another point
    pub fn distance(self: *const Point, other: *const Point) f32 {
        const dx = self.x - other.x;
        const dy = self.y - other.y;
        return @sqrt(dx * dx + dy * dy);
    }
};

pub fn main() void {
    const p1 = Point{ .x = 1.0, .y = 2.0 };
    const p2 = Point{ .x = 4.0, .y = 6.0 };
    const distance = p1.distance(&p2);
    std.debug.print("Distance: {}\n", .{distance});
}

test "simple test" {
    var list = std.ArrayList(i32).init(std.testing.allocator);
    defer list.deinit(); // try commenting this out and see if zig detects the memory leak!
    try list.append(42);
    try std.testing.expectEqual(@as(i32, 42), list.pop());
}
