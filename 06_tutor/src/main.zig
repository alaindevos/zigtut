const std = @import("std");
const print = std.debug.print;

const Age = struct { age: i32 };

const Auser = struct {
    name: []const u8 = "Nobody",
    age: u32,
    fn init(myname: []const u8, myage: u32) Auser {
        return Auser{
            .name = myname,
            .age = myage,
        };
    }

    fn printName(self: Auser) void {
        print("{s}", .{self.name});
    }
};

const Months = enum(u8) {
    Jan,
    Feb,
    pub inline fn isJan(self: Months) bool {
        return self == .Jan;
    }
};

const Myunion = union(enum) { i: i32, f: f32 };

fn addMe(a: i32, b: i32) i32 {
    const result = a + b;
    return result;
}

fn printString(s: []const u8) void {
    print("{s}", .{s});
}

pub fn main() !void {
    // Prints to stderr (it's a shortcut based on `std.io.getStdErr()`)
    std.debug.print("All your {s} are belong to us.\n", .{"codebase"});

    // Data types
    const age: u32 = 10;
    print("{}\n", .{age});
    const f: f32 = 10.0;
    print("{}\n", .{f});
    const b: bool = true;
    print("{}\n", .{b});

    var u: Age = Age{ .age = 32 };
    u.age = 64;
    print("{}\n", .{u});

    const auser: Auser = Auser.init("Alain", 42);
    print("{s}\n", .{auser.name});
    auser.printName();

    const month: Months = .Jan;
    print("{}\n", .{month});

    const myunion: Myunion = Myunion{ .i = 32 };
    print("{}\n", .{myunion});
    switch (myunion) {
        .i => |myi| {
            print("{}\n", .{myi});
        },
        .f => |myf| {
            print("{}\n", .{myf});
        },
    }

    const mysum = addMe(3, 5);
    print("{}\n", .{mysum});
    printString("Hello\n");

    const anum: i32 = 16;
    if (anum < 12) {
        printString("You are too young\n");
    } else if (anum > 40) {
        printString("You are too old\n");
    } else {
        printString("OK\n");
    }
    switch (anum) {
        1...11 => printString("You are too young\n"),
        40...100 => printString("You are too old\n"),
        else => printString("OK\n"),
    }

    const array = [_]i32{ 2, 4, 6, 8, 10 };
    for (0.., array) |i, elem| {
        print("{} {} \n", .{ i, elem });
    }
    var x: i32 = 0;
    while (x < 5) : ({
        x = x + 1;
    }) {
        print("{}  \n", .{x});
    }

    const n = "Alain";
    const n3 = n ++ n ++ n ++ "\n";
    printString(n3);

    //------------------------------------------------------------------------
    // stdout is for the actual output of your application, for example if you
    // are implementing gzip, then only the compressed bytes should be sent to
    // stdout, not any debugging messages.
    const stdout_file = std.io.getStdOut().writer();
    var bw = std.io.bufferedWriter(stdout_file);
    const stdout = bw.writer();

    try stdout.print("Run `zig build test` to run the tests.\n", .{});

    try bw.flush(); // don't forget to flush!

}

test "simple test" {
    var list = std.ArrayList(i32).init(std.testing.allocator);
    defer list.deinit(); // try commenting this out and see if zig detects the memory leak!
    try list.append(42);
    try std.testing.expectEqual(@as(i32, 42), list.pop());
}
