const std = @import("std");
const print = std.debug.print;

pub fn main() !void {
    print("Hello World1\n", .{});
    const ar = [3]i32{ 1, 2, 3 };
    print("Index 1 : {d}\n", .{ar[1]});

    const config = .{ .safety = true };
    var gpa = std.heap.GeneralPurposeAllocator(config){};
    const galloc = gpa.allocator();
    const arrayList = std.ArrayList;
    var b = arrayList(i32).init(galloc);
    defer b.deinit();
    try b.append(1);
    try b.append(2);
    try b.append(3);
    b.items[1] = 5;
    print("Pop : {d}\n", .{b.pop()});
    for (b.items) |item| {
        print("{d}\n", .{item});
    }

    const heapalloc = std.heap.ArenaAllocator;
    var arena = heapalloc.init(std.heap.page_allocator);
    defer arena.deinit();
    const aalloc = arena.allocator();
    var a = aalloc.alloc(i32, 5) catch unreachable;
    a[2] = 5;
    for (0..a.len) |i| {
        print(" {} {d}\n", .{ i, a[i] });
    }
}
