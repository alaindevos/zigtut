const std = @import("std");
const print = std.debug.print;
// Define a function type for unary operations
const UnaryOp = fn (a: i32) i32;
// Example unary operations
fn square(a: i32) i32 {
    return a * a;
}
const config = .{ .safety = true };
var gpa = std.heap.GeneralPurposeAllocator(config){};
const galloc = gpa.allocator();
const arrayList = std.ArrayList;

// Higher-order function: applies a unary operation to each element of an array
fn map(arr: []const i32, op: UnaryOp) ?[]i32 { //NOTE :note: function cannot return an error
    var result = arrayList(i32).init(galloc);
    // defer result.deinit();
    for (arr) |elem| {
        try result.append(op(elem)); // ERROR : expected type '?[]i32', found 'error{OutOfMemory}'
    }
    return result.items;
}

pub fn main() void {
    const numbers = [_]i32{ 1, 2, 3, 4, 5 };
    // Map the 'square' function over the array
    const squared = map(&numbers, square);
    print("Squared: ", .{squared});
}

test "simple test" {
    var list = std.ArrayList(i32).init(std.testing.allocator);
    defer list.deinit(); // try commenting this out and see if zig detects the memory leak!
    try list.append(42);
    try std.testing.expectEqual(@as(i32, 42), list.pop());
}
